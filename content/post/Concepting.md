---
title: Concepten + Presenteren
date: 2018-03-16
---

Vandaag hebben we verder gewerkt aan onze toekomstige lifestyle dairy. We hebben allerlei ideeën bedacht en samen een COCD-box gemaakt.

Om 11:30 moesten Robin en ik een presentatie geven over de lifestyle dairy en de ontwerp criteria. We kregen als feedback van een andere groep dat we te vaak naar de powerpoint keken en dat we meer contact moesten maken met het publiek. Fedde en Nikki hebben er gelijk een validatie formulier aan gekoppeld, zodat we deze konden gebruiken om de competentie Samenwerken af te ronden.

Hieronder elke sfeer beelden van onze presentatie

![enter image description here](https://lh3.googleusercontent.com/dZP9naYWGh8H3B3gONR7DkEMCDan9OVgaJrbeaWIUaBQu_An8pfB6LmVChxkJ7TRcugGEVzsW_5w)
![enter image description here](https://lh3.googleusercontent.com/ZcxSKdaBDxE5p9xl3XBHcZCtI12VN-1ButZ-GA8te3RRzkCbLfxbt0EyDI86F6lsc083PY7gpe-7)