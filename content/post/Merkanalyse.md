---
title: Merkanalyse
date: 2017-11-21
---
## Takenverdeling

Omdat we met zn 6en in een groep zitten, was het voor ons lastig om de taken te gaan verdelen. Er stonden namelijk maar 2 producten op de studioplanning van deze week.
Na overleg hebben we besloten dat Michael en Robin de Debrief gaan maken, en ik samen met Isaiah de merkanalyse gaan opstellen voor PAARD. Nadat we hebben gekeken naar het Merkanalysemodel dat was uitgedeeld, hebben we doormiddel van het beleidsplan van PAARD veel informatie kunnen krijgen.
Ook hebben we vooruit gekeken, en 3 low-fid prototype onderwerpen besproken die je hieronder kunt vinden.

### App acties en aanbiedingen
De gebruiker kan de app downloaden van Paard. In de app kan de gebruiker zijn puntenbalans en de kortingen die hij met die punten kan krijgen checken. De punten verdient hij/zij bij het bezoeken van een evenement. Verder kan de gebruiker de agenda bekijken.
### Een digitale garderobe
Om te beginnen moet de gebruiker kiezen tussen twee opties: Jas ophangen of spullen opbergen in een kluisje. Wanneer de gebruiker heeft gekozen krijgt hij de code waar hij of zij zijn/haar spullen op kan bergen en komt de token uit het gleufje onder de scherm. De gebruiker scant het haakje of kluisje om het te openen om zijn spullen op te bergen. Als de gebruiker klaar is scant hij/zij nog een keer om het haakje of kluisje te sluiten. 
### Aanbevolen evenementen
Als de feestganger naar buiten gaat, loopt hij tegen een grote scherm aan met aanbevolen evenementen die dezelfde genre hebben als de evenement waar hij net is geweest. Op het scherm vind je informatie van de artiest, de datum en de prijs van de event. Via het scherm kan de gebruiker de app en de website bezoeken en krijgt hij een Spotify link gestuurd op zijn/haar telefoon om naar een afspeellijst te luisteren van de gerelateerde artiest.

Mijn keuze gaat uit naar de app met acties en aanbiedingen, omdat ik denk dat vele anderen graag het uitgaansleven een stukje goedkoper willen maken. Ik heb er dan ook vor gekozen om hier een low-fid prototype van te gaan maken.