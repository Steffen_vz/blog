---
title: Mindmappen
date: 2018-02-20
---

Vandaag hebben we de planning besproken voor de komende weken samen met het team. We hebben twee mindmap’s gemaakt met de groep, een over de gezonde levensstijl en de ander over de ongezonde levensstijl

Vandaag hebben we ook een workshop inrichten ontwerpproces gedaan met het team. Doormiddel van technieken die op kaartjes stonden en onderzoek die we hebben verricht op internet, zijn we op de resultaten hieronder gekomen.

Hieronder foto's van de resultaten van de workshop

![enter image description here](https://lh3.googleusercontent.com/IKW2NGxS58LFMuSKZIj_2ZoPt4J4uhzoGgYsfcUf2INQB4plaI60kDC-yu7E_JPzX_g4K1sVgwQJ)
![enter image description here](https://lh3.googleusercontent.com/JWAzJH13i28EYeNLTOlAO9xZ2tKLw72y447x1RjWQMLOKKDorixTvZhGFTyzx_X9JzTtnaJ-FG5S)