---
title: Start Design Challenge 1
Date: 2017-09-05
---

## Briefing 

Nadat we de informatie hebben ontvangen van de eerste zogenoemde deliverables, zijn ik en mijn team begonnen met brainstormen naar onze doelgroep. Omdat we niet precies wisten hoe we onze doelgroep moesten bepalen zijn we op de Social Work studenten gekomen. Daarna hadden onder meer ik en de anderen gelijk ideeën hoe het spel eruit moest gaan zien, wat achteraf niet handig bleek te zijn. Ikzelf had erg veel behoefte om een stappenplan op te stellen, zodat we stapsgewijs door het project heen konden. Dit was niet het geval.

Een aantal dingen waren al wel duidelijk geworden deze week:
1.	Dyann is aangewezen tot captain van ons team
2.	De doelgroep zouden de Social Worker studenten worden
3.	Er is door mij een lijst met teamafspraken opgesteld, met daarin 9 afspraken waar we ons de komende weken aan hielden
4.	Ook hebben we met z’n allen de Belbin test afgenomen, waaruit bleek dat ik samen met Celine de ‘Planten’ van de groep zijn.

## Bijlage:

Teamafspraken

1.	We communiceren elke dag met elkaar via een WhatsApp groep, om elkaar te peilen hoever iedereen is met zijn taken voor die dag. 
2.	We werken in kleine groepjes van 2 aan verschillende taken die gedaan moeten worden, zodat wanneer iemand ziek is, de ander verder kan gaan met de taken.
3.	Als we iets niet willen doen of niet leuk vinden, vragen we hulp aan elkaar.
4.	We communiceren over wat ons dwarszit, zodat we deze problemen samen kunnen oplossen.
5.	Elke Studiodag beginnen met een bespreking/vergadering. 
6.	De Teamcaptain opent de vergadering door te beginnen met de doelen te bespreken die het team voor de dag had ingepland. 
7.	Daarna worden de taken verdeeld voor de dag en wordt er elke dinsdag een globale planning gemaakt. 
8.	We zijn serieus bezig met de challenge, maar er moet ook gewoon gelachen worden!
9.	Iedereen zijn ideeën worden aangehoord door het team, er zijn nooit slechte ideeën.
