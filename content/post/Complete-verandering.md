---
title: Complete Verandering
date: 2017-09-09
---

## Doelgroep

Wat ons opviel was dat we heel weinig informatie konden vinden over de Social Workers. We hadden slecht contact en hebben de keuze genomen om een andere doelgroep te kiezen. We kwamen op het idee om de International Business & Management studenten als doelgroep te kiezen. Onze verwachtingen zijn namelijk dat omdat het een internationale studie is, er veel buitenlandse studenten op de opleiding zitten. Zij zijn vrij onbekend met de stad Rotterdam, denken we. 

Ik heb een vragenlijst opgesteld om een aantal interviews af te nemen bij de IBM studenten af te nemen. Deze staan in de bijlage. Uit de resultaten van de geïnterviewde bleek dat ze graag voor een uitdaging wilde staan als zij een spel speelde. Ze wilden graag medestudenten en de opleiding zelf leren kennen. Ook moest het spel een Storyline bevatten, die je door de game heen begeleidt.

Ook heb ik een moodboard gemaakt over de IBM studenten. Ik heb de foto’s allemaal van het internet af gehaald. 

Als afsluiter van de post wil ik vertellen dat we druk bezig zijn geweest met het bepalen van ons thema. Na enig overleg met studie coach Fedde, zijn we tot de conclusie gekomen dat we Stadsvernieuwing willen laten zien. Zo laten we een andere onbekende kant van de stad zien, waar ook de Nederlanders op de studie IBM wat aan hebben.

## Bijlage:

 Het in kaart brengen van een thema en doelgroep met behulp van een enquête.

Vragen:
1. Leeftijd
2. Geslacht
3. Woonplaats
4. Vooropleiding
5. Wat verwacht je van een introductie?
6. Wat vind jij het meest belangrijke aan een game?
7. Welke spellen heb je de laatste tijd gespeeld (bordspellen, online games, kaarten)
8. Als je een spel speelt, speel je deze binnen/buiten en actief/passief
9. Vindt je rivaliteit in een spel belangrijk?
10. Wat zijn de laatste 3 spellen waar je van hebt genoten?
11. Wanneer verlies je interesse in een spel?
12. Wat maakt Rotterdam voor jou interessant?
13. Zou je meer willen weten over de stad Rotterdam?
14. Zou je liever met een docent/peercoach/alleen met groep het spel willen spelen?

# Moodboard: 
![](https://lh3.googleusercontent.com/-KBRfD-DLcAI/WfL5IkBzzqI/AAAAAAAAAAM/57byxlMi6nM9zTNyw9Cog0IIzG6zht5rQCLcBGAs/s0/Schermafbeelding+2017-10-27+om+11.07.18.png "Moodboard")
