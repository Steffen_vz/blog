---
title: Een nieuwe kans
date: 2017-09-26
---

## Spelanalyse 

We zijn met het team begonnen over de indeling van het spel. We hebben met zn allen een longlist en een shortlist gemaakt. Dit zijn de ideeen voor het spel die:
-	Niet realiseerbaar zijn
-	Realiseerbaar zijn
-	Gewone ideeën
-	Originele ideeën

![Longlist](https://lh3.googleusercontent.com/-yR-gs9KgVw8/WfMjEsjTdBI/AAAAAAAAAA0/cBCl59ZnVvgDOnrG8Wp8HQrfe5JyCOG7ACLcBGAs/s0/IMG_2682.JPG "Longlist")

Uiteindelijk zijn we op ons nieuw bedachte spel gekomen, door er een shortlist van te maken.

![shortlist](https://lh3.googleusercontent.com/-ikM3zjkcHdM/WfMjS73dhvI/AAAAAAAAAA8/R8PaUIKhLvU8suA1t7reSSwxssRVmzlOQCLcBGAs/s0/IMG_2680.JPG "shortlist")

Ook heb ik in deze periode een Spelanalyse gemaakt over een relevant spel: Call of Duty WW2. De resultaten kun je hieronder vinden.

## Spelanalyse: Call of Duty WW2 – BETA VERSION – XBOX ONE

Call of Duty: WWII gaat terug naar de wortels van de franchise waarin de setting van het spel terugkeert naar de Tweede Wereldoorlog. In het spel volgt de speler de veldslagen die de geallieerden uitvochten in West-Europa, waaronder ook de Slag bij Normandië en de Slag om de Ardennen. Het spel draait om de broederschap die ontstond tussen de soldaten in de oorlog.

# Observatie

Het spel vereist enige kennis met de Xbox controller. Hierdoor ontstaan er verschillen in de game-ervaring. Call of Duty WW2 is een game in de Call of Duty reeks. Spelers die de vorige delen van de game hebben gespeelt hebben dus een voordeel, omdat zij de controls van de vorige Games begrijpen. De in-game regels zijn over het algemeen duidelijk. Zodra een beginner de spelregels leest, zal er enige onduidelijkheid ontstaan met verschillende acties die je kan doen in-game. Ervaren Call of Duty gamers zullen de kleine aanpassingen snel begrijpen.

Het kost zeker moeite om de spelregels te snappen, maar door het aandachtig doornemen van de spelregels wordt de game interessanter gemaakt en ga je je inleven in het spel. Ervaren spelers begrijpen de spelregels dus beter bij dit spel. Tijdens het spelen van het spel kan je bepaalde levels verdienen door je tegenstanders in verschillende game-modes te verslaan, in de vorm van een vuurgevecht. Als je meerdere tegenstanders de nek om draait om zo maar te noemen, ontvang je een zogenaamde Score streak. Dit is een voordelige actie voor jouw actie figuur waardoor je gedreven wordt om niet dood te gaan door de tegenstander. 

De spelers worden meestal killers genoemd: het draait er immers om dat je de tegenstander neerschiet. De spelers hebben dus plezier door het vernietigen van de tegenstander. Ook wordt er plezier gemaakt door te socializen. Spelers kunnen doormiddel van een headset communiceren met hun teamgenoten.

Het spel zit goed in mekaar, omdat je meegesleept wordt in de verhaallijn. Mede doordat je samen met je teamgenoten de game aan het spelen bent, met daarbij de audio- en visuele feedback die je van de game krijgt, is de game een goede aanrader. Ook de kwaliteit van het spel heeft effect of je het spel goed of niet goed omschrijft. 

Spelers veranderen van gedrag wanneer zij veel slachtoffers hebben gemaakt naar de tegenstander en zelf niet vaak het slachtoffer zijn geweest. Spelers gaan reageren tegen andere teamgenoten, en zijn meer oplettend. Als de speler vaak het slachtoffer is in het spel, wordt de speler geïrriteerd door zijn eigen en andere speelkwaliteiten. Dit kan zorgen voor grof taalgebruik naar andere spelers of het verlaten van de lobby/game omgeving.

# Analyse

Het Thema is Wereldoorlogen, dit kan je zien aan de aankleding van de game: je speelt met ouderwetse wapens, gebouwen en props (attributen in het spel) zijn verwoest, of zitten granaatscherven in om de situatie na te bootsen. De doelgroep van dit spel zijn fanatieke gamers die graag binnen zitten en alleen spelen. Zij ontmoeten andere spelers via het internet. 

Het spel verloopt als volgt: jouw team dat bestaat uit 6 personen moet de vlag veroveren van de tegenstander, die ook met zn 6en zijn. Doormiddel van het doden van de tegenstander ontvang je punten en heb je kans om de vlag te pakken. Wanneer de vlag is gepakt, moet deze terug worden gebracht naar de basis om een punt te verdienen. De speler die met de vlag rondloopt mag niet doodgaan. Wie er na een bepaalde tijd de meeste vlaggen heeft veroverd, wint het spel.

De magic circle bestaat uit een zogenoemde MAP, die uit een divers aantal spelattributen bestaat. Een spel speel je met maximaal 12 personen met een gemiddelde tijdsduur van 10 minuten.  

# Conclusie

Call of Duty is voor de beginnende console gamer een uitdagend spel wanneer hij het spel interresant gaat vinden. Het spel wordt op meerdere platformen gespeelt, waaronder de ps4, Xbox one en op de PC. Het is een echte killer game met daarbij de symptomen dat de gebruiker alleen speelt en binnen zit. 

