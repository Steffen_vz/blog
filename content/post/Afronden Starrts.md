---
title: Inleveren Leerdossier
date: 2018-03-30
---

Omdat ik mijn planning voor de afgelopen week heb aangehouden, en ik wist dat ik naar het ziekenhuis moest, hoefde ik vandaag weinig meer te doen voor het Leerdossier. Ik heb anderen teamleden nog kunnen helpen met het schrijven van hun Starrts, en hun documenten kunnen aanleveren zodat zij ook spoedig hun Leerdossier konden inleveren.

Ik kijk met een goed gevoel terug naar het afgelopen kwartaal. Ik heb mij gericht op 4 Starts die ik wil afronden op de competentietafels, zodat ik volgende keer me op de andere 4 kan richten. Ik wil minimaal 3 competenties halen en ga ervoor zorgen dat ik goed voorbereid ben op de Cometentie tafels.

Ik wil nu alvast Sharon, Selen en Robin bedanken voor de sfeer die we in de laatste tijd binnen de groep hebben gehad. Ik hoop dat we allemaal zoveel mogelijk competenties kunnen halen en hoop dat we met zn allen door kunnen naar jaar 2.

Dit was het einde van de blog in kwartaal 3.