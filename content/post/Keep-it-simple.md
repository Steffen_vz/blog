---
title: Keep it simple!
date: 2017-10-04
---
## Prototype

Ik ben druk bezig geweest voor iteratie 2 met het prototype. Ik heb een telefoon van papier gemaakt en heb meegeholpen om de minigames uit te werken, zodat we deze konden presenteren. 

Helaas kon ik bij de presentatie er niet bij zijn doordat ik ziek was. Wel heb ik van mijn teamgenoten de feedback ontvangen zodat we voor de laatste iteratie ons spel kunnen verbeteren. Keep It Simple! Was de uitspraak van Bob. We moesten niet veel meer veranderen aan het spel en alleen dingen beter uitwerken. 

Hieronder de ontvangen feedback van de presentatie:

- Harder praten
- Sterker offlinecomponent
- Ook de mensen achterin de klas aanspreken in plaats van alleen focussen op de voorste rij of de docent
- Potlood is niet goed zichtbaar
- De vragen in het spel moeten moeilijker zijn; de naam van het spel is niet voor niets ‘headcrackers’
- Onderzoek doen naar hoe groot het percentage buitenlandse studenten is.
- De app moet in het Engels
- Onderzoek doen naar hoe het zit met het internationale karakter
- Onderzoek doen naar hoeveel leerlingen er na de opleiding daadwerkelijk naar het buitenland gaan
- Betere inleiding van het spel
- Preciezere locatie geven in het spel waar de leerlingen moeten zijn
- Meer informatie geven over de beweging (havens!) van de stad. Dit is namelijk door de Maasvlakte erg verschoven.
- De verbeterpunten en de spelanalyse moeten in de presentatie in plaats van dat er naar gevraagd moet worden.

## Foto's:

![enter image description here](https://lh3.googleusercontent.com/-sA5G-WAGPQw/WfMpPQseB_I/AAAAAAAAABY/1m3K4UawurQtZs74KEPnsai09wOReC_ggCLcBGAs/s0/IMG_3102.JPG "IMG_3102.JPG")

![enter image description here](https://lh3.googleusercontent.com/-aTdrLFy1AYY/WfMpUC6S4II/AAAAAAAAABg/v3QXTysh-sspIRvp8bd8LDtSS5c4o14dQCLcBGAs/s0/IMG_3099.JPG "IMG_3099.JPG")

![enter image description here](https://lh3.googleusercontent.com/-LU83YHybeAw/WfMpZpIcsLI/AAAAAAAAABo/DIdafDK8aZIZ11xev2Z2DbSizg7t4kPwACLcBGAs/s0/IMG_3101.JPG "IMG_3101.JPG")