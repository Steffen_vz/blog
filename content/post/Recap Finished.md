---
title: Recap afmaken
date: 2017-12-19
---
## DRUK DRUK DRUK
Vandaag zijn we erg druk bezig geweest met alle informatie te verzamelen voor de Recap en deze te valideren. Omdat iedereen zijn eigen aandeel heeft geleverd, is de communicatie tussen elkaar erg belangerijk. Dit was helaas niet het geval en dus moesten er nog een aantal dingen worden veranderd, toegevoegd.
De feedback die we hebben ontvangen was kritisch:
- Veel tekst; het mag allemaal wat minder uitgebreid
- Meer plaatjes toevoegen; we gebruikten 3/4 plaatjes per pagina
- Behoud structuur; het was onduidelijk waar je moest kijken
Deze feedback hebben we verwerkt en de resultaten kan je hieronder vinden.

## Foto's
![enter image description here](https://lh3.googleusercontent.com/-XT6cfDYZwfM/WlyxaONW4oI/AAAAAAAAG6Q/ae1lU4oMq4gTlN8YM41JgyXZ_BSjObbWACLcBGAs/s0/Schermafbeelding+2018-01-15+om+14.47.50.png "Schermafbeelding 2018-01-15 om 14.47.50.png")

![enter image description here](https://lh3.googleusercontent.com/-mdtG6L_wcg8/WlyxUDL6OuI/AAAAAAAAG6I/LV1KVp8ENog80876Dc8tNfcj1ue2AXzuACLcBGAs/s0/Schermafbeelding+2018-01-15+om+14.48.06.png "Schermafbeelding 2018-01-15 om 14.48.06.png")