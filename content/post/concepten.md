---
title: Concepting
date: 2018-04-18
---

Concensus voor een concept

Vandaag moesten we echt een concept bedenken, we hebben namelijk nog maar 2 weken voordat we ons high fit prototype moeten presenteren op de expositie.
Het was erg moeilijk en we bleven hangen op 1 idee. Namelijk door boetes te geven aan mensen die te hard rijden door de wijk. En met die boetes wordt geld ingezameld voor een buurthuis. Toen we dit met fedde bespraken gaf hij aan dat we niet zomaar boetes kunnen uitdelen. Vervolgens hadden we bedacht om mensen op een positieve manier te beïnvloeden door iedereen die wel een goede snelheid aanhoud te belonen. We wilde in het midden van de Beijerlandselaan een paal zetten die gaat kleuren als de goede snelheid wordt aangehouden. Maar uiteindelijk vonden de meeste van ons dit niet linken aan gezondheid van jongeren. Ik merkte dat de groep erg gedemotiveerd werd omdat er nog geen concept was.

Lisanne had toen voorgesteld dat we ons moeten focussen op de Turkse gemeenschap, hierdoor is onze doelgroep specifieker. Vervolgens zijn we gaan denken over het verschil tussen Turkse ouders en Nederlandse ouders. Uiteindelijk was mijn groep er ook mee eens dat Turkse ouders meer gezag hebben over hun kinderen. Hierdoor ontstond het idee om via de ouders de jongeren gaan inspireren. Doordat we de ouders meer kennis en inzicht laten geven, kunnen zij hun jongeren motiveren en inspireren. Toen is Lisanne met een geweldig idee gekomen om een tijdschrift te gaan maken en was de groep weer enthousiast en zo is ons concept idee ontstaan.