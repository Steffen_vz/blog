---
title: User Journey 2
date: 2017-11-28
---

## Probleem
Doordat we volgens de ontvangen feedback niet goed gericht waren op het PAARD, maar de algemene CLub, hebben we opnieuw een User Journey moeten maken. We kregen nog niet echt een goed beeld van de ervaring.
Nadat we de User Journey volledig hadden en we er een goed beeld van kregen, hebben we het gevisualiseerd. De uitwerking hiervan kun je hieronder vinden.
## Foto's
### Voor het event
![-](https://lh3.googleusercontent.com/-aYfrMNJ04nQ/Wlx6HqqYVGI/AAAAAAAAG2E/iydFihSGVqwYMM-DPcgvUJioIwBw6EErwCLcBGAs/s0/Midde.JPG "Midde.JPG")
### Tijdens het event
![-](https://lh3.googleusercontent.com/-XTPARcp0yMQ/Wlx5wANLFlI/AAAAAAAAG18/h5sWMOdshA8GODEKUbgcLWlW8N66XD9mgCLcBGAs/s0/During+user+journey.JPG "During user journey.JPG")
### Na het event
![-](https://lh3.googleusercontent.com/-fPk-icFwmVU/Wlx6PSeB0fI/AAAAAAAAG2Q/ylxkxOyNmnkELHdz3y8tpR4xSlIId69GQCLcBGAs/s0/After+user+journey.JPG "After user journey.JPG")
### User Journey gedigitaliseerd
![-](https://lh3.googleusercontent.com/-miDT_FM1tao/Wlx77iyFXFI/AAAAAAAAG2o/7buybXunMwYlJmys1-O0Pl5qw1xbCEvnACLcBGAs/s0/Schermafbeelding+2018-01-15+om+10.57.50.png "User journey")