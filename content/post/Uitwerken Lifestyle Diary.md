---
title: Uitwerken Lifestyle Diary
date: 2018-03-06
---

Na de Vakantie ben ik papier gaan halen bij de Willem de Koning, om mijn foto's die ik heb genomen in mn Lifestyle Diary te zetten. Ik heb een drukke vakantie achter de boeg gehad, omdat ik graag wilde werken om geld te sparen om met de zomer op vakantie te gaan!

We hebben even teamoverleg gehad en iedereen even bijgepraat over de stand van zaken. Vrijdag moet de Lifestyle Diary ingeleverd worden.

Ook heb ik een workshop gedaan over basistechnieken interviewen van Pia. Ik heb geleerd dat een interview meer is dan je vragen stellen die je hebt opgesteld. Ik heb samen met Robin 5 vragen opgesteld over gezondheid die we aan elkaar moesten stellen en zo goed mogelijk proberen in het gesprek blijven. We hebben 10 minuten kunnen praten met de 5 vragen die we hadden opgesteld. Ik heb er erg veel baat bij gehad!