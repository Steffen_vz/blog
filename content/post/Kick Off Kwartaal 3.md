---
title: Kick Off Kwartaal 3
date: 2018-02-06
---

## Kick off
Op Dinsdag 6 februari hadden we de nieuwe kick-off van kwartaal 3 & 4. Voor de opdracht van dit kwartaal werken we samen met Emi (Centre  of Expertise Social Innovation). De kernvraag van het project leidde als volgt: “Hoe kun je jongeren in Rotterdam (Zuid) stimuleren tot het aannemen van een gezonde leefstijl?”

De opdracht bestaat uit twee delen. Het eerste deel gaat in op de (gezonde) leefstijl van studenten aan Hogeschool Rotterdam. Het tweede deel focust zich op de leefstijl van de jongeren in Rotterdam Zuid.

Ook hebben we informatie ontvangen over de Maastrichtreis. We zouden een voorproefje ontvangen van het project dat we moesten gaan uitwerken. Ik had er erg veel zin in, omdat ik had gehoord dat vele andere klasgenoten ook naar Maastricht zouden gaan.
