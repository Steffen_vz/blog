---
title: Start Kwartaal 4
date: 2018-04-10
---

Vandaag hebben we de nieuwe opdracht doorgenomen met het team. In deze periode staan de jongeren van Rotterdam Zuid centraal, in plaats van de gezondheid van jongeren op Hogeschool van Rotterdam. We hadden keuze tot verschillende wijken, en we hebben ervoor gekozen om ons te richtten op Hillesluis. Sharon woont namelijk in de wijk en Selen heeft ook in deze wijk gewoont. Zo kunnen we ons beter verdiepen in de doelgroep, omdat er al sociale contacten zijn opgebouwd met de jongeren. Er is gelijk door Selen en Lisanne een planning voor de komende weken gemaakt. Ook hebben we met het team de taken voor het plan van Aanpak vastgelegd. 
Via deze link hieronder kun je een kijkje nemen in onze planning:
https://docs.google.com/spreadsheets/d/19CQ7JfhYSyRWiG0BykKFJt_i6ENE747T16A600PQP5Y/edit#gid=0
Deze planning zal in de aankomende weken worden verbeterd door Selen!
