---
title: Design Challenge 2
date: 2017-11-17
---

Nou, op naar de volgende Challenge. Mijn eerste reactie toen ik zag dat we een project gingen maken voor een Club in Den Haag, was ik verrast en nieuwsgierig over Het zogenoemde PAARD. Wat mij gelijk opviel was de bereikbaarheid van het PAARD. Het gebouw zat erg verschut in de straat. Je kon de ingang slecht zien. 
Nadat we de presentatie hadden gehad, heb ik nieuwsgierig gekeken naar de Film die werd gedraaid na de pauze. Het ging over 'What is Design?' waarin vele aspecten van de Design industrie in terugkwamen bij de grote bekende merken, onder andere Coca Cola.
Op school hebben we aan de hand van elkaars sterke punten van de Belbin test groepen gemaakt. Onze groep heet Smooth Operators. 

Ook hebben we een SWOT analyse gemaakt, en een Samenwerkingsovereenkomst, die je hieronder kunt vinden.

![enter image description here](https://lh3.googleusercontent.com/-C3g034zxQ4c/WlYoAQgldBI/AAAAAAAAGzw/57BlHcmFj5Er-QHcNVRQD-l8EtcX_f4qQCLcBGAs/s0/Schermafbeelding+2018-01-10+om+15.47.35.png "swot")
![enter image description here](https://lh3.googleusercontent.com/-w3sHihA1mXI/WlYpD_N_DeI/AAAAAAAAGz8/bV8hT-VYMIIDPTMhgp6eBxx3Qv69BohSgCLcBGAs/s0/Schermafbeelding+2018-01-10+om+15.53.10.png "Schermafbeelding 2018-01-10 om 15.53.10.png")
![enter image description here](https://lh3.googleusercontent.com/-h7khhZr9TL0/WlYpgAxTnoI/AAAAAAAAG0I/hwr09pUb6CY9gMawrI47elZ8orVeV9P5gCLcBGAs/s0/Schermafbeelding+2018-01-10+om+15.53.19.png "Schermafbeelding 2018-01-10 om 15.53.19.png")
![enter image description here](https://lh3.googleusercontent.com/-n1hW4fibnxU/WlYpzn5243I/AAAAAAAAG0U/IEO4wsfRbPYPXyczFNTJwCPMIl7BQdZqwCLcBGAs/s0/Schermafbeelding+2018-01-10+om+15.53.41.png "Schermafbeelding 2018-01-10 om 15.53.41.png")
