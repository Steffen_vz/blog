---
title: Start iteratie 2
date: 2017-09-19
---

## Presentatie

Vandaag is de dag dat we ons ontwerp moeten presenteren. We hadden vooraf bepaald dat ik zou gaan presenteren met Robin. Omdat er toch wat onduidelijkheden waren over ons ontwerp, om dat de communicatie tussen ons even niet helemaal klopte, hebben we met zn allen op Dyann na de presentatie gegeven. We kregen goede feedback terug, met als belangrijkste punt: Wat verbindt de doelgroep met het thema. Dit hadden we door vertraging niet goed kunnen beantwoorden.

Na de presentaties kregen we te horen dat we ons hele idee moesten vergeten. Dit wordt in de designwereld Kill your Darlings genoemd. We hadden er al veel tijd en werk ingestoken om ons project weer te gaan verbeteren. Gelukkig kwamen we met zn allen gelijk op nieuwe ideeen, die afweken van ons vorige ontwerp. Er is een nieuwe taakverdeling gemaakt, en zijn aan het vergaderen geweest over ons nieuwe ontwerp.
