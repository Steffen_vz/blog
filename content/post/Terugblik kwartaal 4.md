---
title: terugblik kwartaal 4
date: 2018-06-03
---

Ik heb me dit kwartaal goed kunnen focussen op de cometenties Evalueren van ontwerpresultaten en Empathie. Wat me het meeste is bijgebleven is dat ik met het team de afgelopen weken hard heb moeten werken, en dat dit ook mogelijk is gemaakt door de goede samenwerking. Zo wil ik iedereen bedanken en succes wensen met het assesment, en hoop ik dat ik iedereen kan zien in jaar 2.

Speciaal wil ik Robin bedanken, mede omdat ik alle 4 kwartalen met hem samen gewerkt heb, maar ook omdat we elkaar goed begrijpen en we elkaar ook van kritiche feedback kunnen voorzien!

Bedankt voor dit leerzame jaar, en ik zal begin september beginnen met een nieuwe, verse blog!

Mvg, Steffen van Zuijlen CMD1G