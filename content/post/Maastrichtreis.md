---
title: Maastrichtreis
date: 2018-02-07
---

## DAG 1
Op woensdag 7 februari zijn we met vele 1e jaars CMDers naar Maastricht geweest. We waren rond 9:30 en 10:00 vertrokken en kwamen daar rond 12:00/12:30 aan.

In de bus is er besloten dat ik samen met Isaiah, Shreyas, Michael, Robin K en Robin A in de kamer zit de aankomende dagen. Op onze eerste dag moesten we groepen maken doormiddel van een eigen persona te tekenen als superhelden, en de groepen werden daarna gekozen door de peercoaches. Ik had het anonymous masker nagemaakt, die je hieronder kan bekijken. We kregen een presentatie van Raul Martinez over onze opdracht en een inleiding van onze aankomende project over gezonde leefstyle. De bedoeling was dat we aan de hand van het BRAVO model en de gekregen doelgroep, een gezonde oplossing moesten vinden. We gingen de stad in om studenten van 18 t/m 25 jaar te interviewen en in de avond hadden we een gezamenllijke BBQ!

De volgende dag hadden we om 8:00 ontbijt en om 10:00 moesten we weer verzamelen zodat we weer verder konden gaan met het project in Maastricht. In de avond moesten we onze concepten presenteren en hadden we de avond voor ons zelf om Maastricht te verkennen. 

In de ochtend hadden we ontbijt en konden we ons weer klaarmaken voor de terugreis naar Rotterdam.

Ik heb het erg naar mijn zin gehad en zou graag vaker met excursies willen deelnemen.

Ik heb hieronder een paar foto's genomen van de Maastrichtreis, om een kleine impressie te krijgen van de opdracht en de resultaten!
![enter image description here](https://lh3.googleusercontent.com/E8Uak6MkByzvK1WYcrDqX_b0ZwAqSxK5YfneVmghCLUWRcEfThCemdl-eKyOF58wbuDUAosX58Fh)
![enter image description here](https://lh3.googleusercontent.com/SQ3yCUCcAxJTNtNPull9SJ90QWuW_0YkNkLejNhg2TW8wzTxpZvJ6zVUojuBhKIdlaqUaLOgw0HP)
![enter image description here](https://lh3.googleusercontent.com/HR_oRwQGdjOXuHVRuTrWM77vTeg9sJ0jZFvgKf1Dwejol-98Q215rSLCzhZBN1BsnH2722HGFMjW)
![enter image description here](https://lh3.googleusercontent.com/81Kc1yp_wd3-EXi0vAd6sbajTiVhuAKRSiOk16bMCq9rA3ajgqkcUYzSrYxLF7hoExXRdm7IPRNu)
![enter image description here](https://lh3.googleusercontent.com/3CX8cxiArMSyoNovPc7zGj1BoRnmnyHafvy0_Vluigo-TRRBqabmdOlZEY0y-6TrdBO5S-IaTWHX)