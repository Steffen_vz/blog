---
title: Recap
date: 2017-12-12
---
## Overzicht zoeken
We hebben met zn allen even duidelijk gemaakt wat we in ons high-fid prototype gaan zetten. Er is een storyboard gemaakt. Dit geeft mij een duidelijk overzicht van wat iedereen van mij verwacht. Ook heb ik een planning gemaakt voor mezelf om in de vakantie te gaan werken aan het prototype.
## Beginnen Recap
Ook zijn we begonnen met de Recap. Ik vindt dit een goede tussenstap van het vervolg van het project, omdat het project op 2 A3 vellen duidelijk wordt gemaakt. Ik heb de taak op mezelf gezet om mijn beroepsproducten erin te verwerken en alle informatie op de vellen papier te zetten. 
![enter image description here](https://lh3.googleusercontent.com/-25Ek0bdBWm4/WlytUf5tCII/AAAAAAAAG5w/NDtYNFqkzhoypm5mf66z7awKJzdLbBZvACLcBGAs/s0/a333.png "a333.png")