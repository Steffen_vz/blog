---
title: Alles op een rijtje
date: 2017-09-16
---
## Spel componenten
We hebben even alle verzamelde informatie op een rijtje gezet vandaag zodat voor iedereen duidelijk is wat ons idee is en hoe we dit gaan aanpakken. Ik ben begonnen met het uitwerken van verschillende spel ideeën. Dit zijn componenten die je je in het spel kan gebruiken. In de bijlage heb ik een foto toegevoegd waarin je kan zien welke spelcomponenten wij het belangrijkste vonden om in het spel te zetten. 

Daarnaast heb ik aan mijn individuele deliverables gewerkt. In mijn Dit ben ik profiel heb ik het volgende beschreven:

Eigen kwaliteiten:
-	Improviseren in bepaalde situaties
-	Erg meedenkend
-	Vol met ideeën die uitgewerkt moeten worden
 
Opleiding:
-	Havodiploma behaald in periode 2016-2017

Werkervaring:
-	Minimale ervaring met fotoshop
-	Bezig met het aanleren van Sketch

Interesses:
-	Ontwerp technieken en het ontwerpen van mijn eigen App.

Wat wil ik bereiken met deze opleiding?
-	Ik heb voor deze opleiding gekozen omdat ik mezelf en anderen creativiteit in mij zien. Dit zou ik willen ontwikkelen, met de gedachte om in de nabije toekomst onderzoek te doen, naar zowel websites en apps, om zo mijn eigen app op te bouwen.

Wat zijn mijn doelen voor het eerste kwartaal?
-	Op mijn gemak voelen in de klas en op school
-	Een basis ritme te maken om mezelf 100% in te zetten
-	Het leren van de basis van het ontwerpen
-	Mezelf zijn in bijzijn van teamgenoten

Uitslag Belbintest: Plant
De plant is een ietwat naar binnen gekeerde, serieuze creatieveling met bij tijd en wijle briljante en onorthodoxe ideeën. Hij of zij weet veel, heeft veel fantasie en is bovengemiddeld intelligent. Maar de plant vindt het moeilijk om zijn ideeën aan de man te brengen. Deels omdat hij wat moeite heeft met andere mensen, deels omdat hij praktische bezwaren het liefst wegdenkt. ‘Dat is uitwerking ...’ vindt een plant. Een plant in z’n eentje komt niet ver. Meer planten zijn al snel de dood in de pot: die hebben te veel ideeën en komen er onderling niet uit.

Ontwikkelmogelijkheden voor een plant
Een plant zou vaak beter tot zijn recht komen als hij wat extroverter zou zijn. Een plant heeft de neiging om in een hoekje te gaan zitten broeden. Dat is mooi maar met de ideeën die hij daarmee genereert, overvalt hij zijn collega’s vaak. En daarmee verliest een plant veel van zijn effectiviteit. Want al die mooie ideeën, die moeten ook worden uitgevoerd. En een beetje plant laat het werken aan een ander over. Een plant doet er daarom goed aan even wat vaker bij een collega binnen te lopen en zijn ideeën voor te leggen. Een plant doet er ook goed aan om wat vaker hardop te denken.

Waar let je op als leidinggevende van een plant?
De leidinggevende zal z’n best doen om de plant wat meer buiten te zetten. Een beetje lucht en licht is goed voor de plant. Je wilt dat de plant wat sneller en handiger met z’n ideeën komt. Dus je probeert hem wat extroverter te maken. Je vraagt de plant wat vaker om een presentatie te houden. In het werkoverleg vraag je als leidinggevende expliciet wat de plant ervan vindt. Je vraagt naar ideeën, je vraagt de plant om die ideeën samen met een collega uit te werken. Kortom: je betrekt de plant meer bij het spel.

Mening Belbin test:
-	Ik vind dat het grootste deel van de omschrijving van de plant weldegelijk klopt. Ik vind mezelf creatief en kom vaak met veel goede ideeën. Ook is het waar dat ik het moeilijk vind om mijn ideeën te verwoorden naar beelden die in mijn hoofd omgaan. Ik heb gemerkt dat mijn groep aardig op 1 lijn zit met ideeën en dat we veel dezelfde denkwijze hebben. Ik zou hardop willen nadenken, maar kan mijn ideeën niet verwoorden.

## Bijlage:

Spel componenten
![richtingspel](https://lh3.googleusercontent.com/-Kgosiz4rvIY/WfMW7TDEJYI/AAAAAAAAAAg/0WGgF5PNVZ0z0yP6fXL5wFRFDP09YauwwCLcBGAs/s0/IMG_2402.JPG)