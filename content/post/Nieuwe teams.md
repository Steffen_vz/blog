---
title: Nieuwe teams maken!
date: 2018-02-13
---

Op 13 Februari hebben we nieuwe teams moeten maken voor zowel het 3e en het 4e kwartaal. Doormiddel van een rol in de groep te kiezen werden er teams gemaakt door de Teamleiders. Ik heb ervoor gekozen om teamleider te worden omdat Fedde mij het in het 1e kwartaal heeft geadviseerd. Ik had al een beetje een idee wie ik graag in mijn team zou willen hebben: Robin, Selen en Sharon. Door een klein beetje geluk te hebben is het gelukt om samen met hun en Amel en Lisanne een team te vormen. 

We hebben gelijk besproken wat we als doel willen bereiken binnen het team: 
- het zoveel mogelijk behalen van competenties in het 3e kwartaal
- goede communicatie binnen het team

We hebben al een kleine planning gemaakt voor de aankomende week, waaronder ik de Debrief ga maken. Vrijdag moet er namelijk worden gepresenteerd!

Hieronder mijn team:

![enter image description here](https://lh3.googleusercontent.com/SeZGjC_Rg7SibsdgCJLTyPH8wOGR17juJJcJOlPao0IUBihr6Nes0m0mpeWzlkmsnrH-dg2LXTdK)
