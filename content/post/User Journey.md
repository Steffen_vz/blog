---
title: User Journey
date: 2017-11-24
---
## Wie is de Selecteur?
Om een beter beeld te krijgen van de gebruiker en het PAARD hebben we een User Journey opgesteld. Wat zijn de actions, painpoints en enjoyable moments, die de gebruiker meemaakt in de tijd dat hij met het onderwerp PAARD bezig is. Aan de hand daarvan hebben we een lijndiagram geschetst. 
Conclusie: Tijdens de beleving die de selecteur heeft in het PAARD, zijn er teveel negatieve punten, waaronder het grootste probleem de Garderobe is.
Ook hebben ik en Isaiah gewerkt aan de Merkanalyse, en de vergelijking opgezocht met de Melkweg, een bekende Club in Amsterdam. Hoe komt het nou dat mensen veel liever naar de Melkweg gaan?
## De Merkanalyse
![enter image description here](https://lh3.googleusercontent.com/-VLcOb6p7aTA/Wlxs-4z4RQI/AAAAAAAAG1Y/hPKJFhvONRoV0mT5E47pPqk4e0ePsArdwCLcBGAs/s0/Schermafbeelding+2018-01-15+om+09.54.30.png "Merkanalyse1")
![enter image description here](https://lh3.googleusercontent.com/-sijBEEOa1cc/WlxtLRqvGeI/AAAAAAAAG1g/Zit5OJuE8Vws-LM-3arynPDV3_rE8vExgCLcBGAs/s0/Schermafbeelding+2018-01-15+om+09.54.16.png "merkanalyse2")