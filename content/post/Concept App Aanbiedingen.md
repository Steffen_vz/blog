---
title: App met Acties en Aanbiedingen prototype
date: 2017-12-01
---

## Prototypen
Vandaag heb ik mijn concept afgemaakt van de app. Ik heb een hoofdmenu gemaakt zodat je vanaf daar verschillende stappen kan ondernemen. Samen met Lisanne heb ik een testplan opgesteld, en de nodige storyboards erbij gemaakt. Ik was erg tevreden over mijn onderwerp. Ik heb er veel tijd in gestoken om mezelf ook te ontwikkelen in mijn Sketch kunsten. 
Uiteindelijk hebben we toch voor het idee van Michael gekozen, omdat met zijn ontwerp we de meeste painpoints konden verhelpen en zijn ontwerp uniek was tegenover de ander groepen in de klas. 
Zelf vondt ik het erg jammer natuurlijk, maar had ook wel erg veel zin om zijn ontwerp te gaan uitwerken als high-fid prototype. Hieronder kun je overigens mijn ontwerp bekijken.

## foto's
### Eigen ontwerp app Acties en Aanbiedingen
![enter image description here](https://lh3.googleusercontent.com/-gSn5W2Hz3co/WlyIJWtj1XI/AAAAAAAAG3I/zYMsucNID1wvglugllG3bGYGp4ZU2W5kACLcBGAs/s0/Schermafbeelding+2018-01-15+om+11.48.40.png "Schermafbeelding 2018-01-15 om 11.48.40.png")
![enter image description here](https://lh3.googleusercontent.com/-A3df8wX3kFc/WlyITnWxFaI/AAAAAAAAG3Q/HhgejOD39ZoWRG3BtdimBnUUtFdYCOEmQCLcBGAs/s0/Schermafbeelding+2018-01-15+om+11.48.55.png "Schermafbeelding 2018-01-15 om 11.48.55.png")
![enter image description here](https://lh3.googleusercontent.com/--dabPThb2Dw/WlyIcWfhUjI/AAAAAAAAG3Y/JzApZS7PBKsHhrlJrGQArTUkPvJuE6IXgCLcBGAs/s0/Schermafbeelding+2018-01-15+om+11.49.08.png "Schermafbeelding 2018-01-15 om 11.49.08.png")
![enter image description here](https://lh3.googleusercontent.com/-BnENLMgEz3I/WlyIlOO6EFI/AAAAAAAAG3g/NAiaMpz_Niwi3CAZON4D4sh6-l1fAGKGACLcBGAs/s0/Schermafbeelding+2018-01-15+om+11.49.21.png "Schermafbeelding 2018-01-15 om 11.49.21.png")
### Dit heb ik gemaakt voor Robin zijn ontwerp
![enter image description here](https://lh3.googleusercontent.com/-DRq4qmQmEek/WlyI3y0HV6I/AAAAAAAAG3s/5hBKr0vGHYww1I1MTJBgolDicXtdNhClACLcBGAs/s0/Schermafbeelding+2018-01-15+om+11.48.14.png "Schermafbeelding 2018-01-15 om 11.48.14.png")