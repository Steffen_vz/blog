---
title: Laatste voorbereidingen voor de vakantie
date: 2018-04-24
---

Dinsdagochtend gingen wij nog even reflecteren op de gegeven presentatie van Vrijdag 20 April 2018. Mijn groepje heeft nog zijn/haar aandeel kunnen verbeteren en ik heb zelf mijn Plan van Aanpak kunnen verbeteren en kunnen inleveren.
Daarna hebben wij als groep nog met ons projectdocent Fedde nog een aantal dingen besproken. Hij wilde weten of we de beroepsproducten goed hadden verdeeld onder elkaar zodat iedereen zijn of haar competenties kan behalen. We hebben aangegeven dat we dit goed hebben onderverdeeld. Daarna heeft Steffen ons allemaal de opdracht gegeven om vragen te bedenken voor ‘vraagkaarten’. De vragen moesten gaan over gezond eten en beweging.

(Aankomende vrijdag was er geen Studiodag, vanwege Koningsdag op 27 April 2018)