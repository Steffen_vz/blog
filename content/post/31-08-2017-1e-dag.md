---
title: 1e werkdag
date: 2017-08-31
---

Om de dag te beginnen hebben we een hoorcollege over Games gehad. Wat houdt gamen nou in en wat zijn belangenrijke speerpunten waardoor je iets een game mag noemen: 
1. Een game heeft 3 verschillende aspecten: 
- De game moet een doel voor ogen hebben
- De game moet regels bevatten
- Er moeten keuzes aan de gebruiker aangeboden worden

2. Er bestaan 4 verschillende soorten gamers:
	- Killers: 
	- Achievers: Zij willen alle doelen in de game behalen
	- Socializers: Zij willen contact leggen met andere gamers
	- Explorers: Zij willen de game helemaal ontdekken, VB Easter eggs.

Na het hoorcollege over games hebben we de eerste Design challenge ontvangen: Maak een Spel dat 1e jaar studenten verbindt met de stad Rotterdam. 